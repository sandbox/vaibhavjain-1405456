-- SUMMARY --

The comment expire module proivides you a feature where you can update
status of comments from read/write to read only of nodes for each content type,
after defined time.
It depends on cron. if the cron is not setup, do not expect this to work.
This will be executed once per day, if cron is setup to run more than one a day.

-- INSTALLATION --

See the installation guide here - http://drupal.org/node/70151

-- CONFIGURATION --

Go to url - admin/content/comment/comment-expire and setup Global setting.
This global setting will effect all the content types, if you want to
override for any content type, you can specify duration for same.
This will make sure global settings will apply for all other content types,
but not for the content types specified.
